﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreetingMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("What is your first name?");
            string a = Greeting(Console.ReadLine());
            
            if (a == "World")
            {
                Console.WriteLine("Oh my god, you're a sentient computer, shit.");
            }
            else
            {
                Console.WriteLine(a);
            }
        }

        static string Greeting(string message)
        {
            return $"Hello {message}!";
        }
    }
}
